#+BABEL: :session *R* :colnames yes

#+BEGIN_SRC R :results silent
source("../R/load.R", chdir=TRUE)
#+END_SRC


#+BEGIN_SRC R  :results silent
  prices <- acwPrices3()
#+END_SRC

** Unit Root Tests

*** ADF and Phillips/Perron

A weak test of an efficient market is that the prices 
follow a random walk.  Like \textcite{LeighWolfersEtAl2003}
I do an Augmented Dickey Fuller and Phillips-Perron test on 
all of my price series.

For each time series, I take all non-missing values, and ignore
the frequency, only considered the order of non-missing values. 
Since there are very few non-missing values I don't think this will 
be a problem.

#+BEGIN_SRC R  
  .tots <- function(x) as.ts(na.omit(x))
  .pptest <- function(x) {
      round(suppressWarnings(pp.test(.tots(x))$p.value), 2)
  }
  .adftest <- function(x) {
      round(suppressWarnings(adf.test(.tots(x))$p.value), 2)
  }
  
  within(ddply(acwPrices3(), .(variable),
               summarise,
               adf=.adftest(value),
               pp=.pptest(value)),
         unitroot <- !(adf < 0.05 | pp < 0.05))
#+END_SRC

#+results:
| variable           |  adf |   pp | unitroot |
|--------------------+------+------+----------|
| greenback1         |  0.8 | 0.92 | TRUE     |
| greenback2         | 0.57 | 0.68 | TRUE     |
| grayback1          | 0.75 | 0.84 | TRUE     |
| grayback_houston1  | 0.77 | 0.82 | TRUE     |
| grayback_houston2  | 0.75 | 0.49 | TRUE     |
| grayback2          | 0.99 | 0.99 | TRUE     |
| grayback3          | 0.99 | 0.99 | TRUE     |
| grayback4          | 0.99 | 0.61 | TRUE     |
| csa_bond_london    | 0.88 | 0.88 | TRUE     |
| csa_bond_amsterdam | 0.25 | 0.47 | TRUE     |
| csa_bond_100mn     | 0.58 | 0.78 | TRUE     |
| csa_bond_15mn      | 0.99 | 0.99 | TRUE     |
| virginia_bond      | 0.39 | 0.58 | TRUE     |
| richmond_bond      | 0.92 | 0.88 | TRUE     |
| virginia_spread    | 0.85 | 0.87 | TRUE     |
| richmond_spread    | 0.98 | 0.99 | TRUE     |
| us_bond1           | 0.98 | 0.99 | TRUE     |
| us_bond2           | 0.78 | 0.65 | TRUE     |


While this is good in that it means all my markets function well, 
a result also found by \textcite{Roll1972,McCandless1996}; it is bad
in that I need to difference to run any regressions.
Should I first difference? or take the first difference of logs? 

#+BEGIN_SRC R  
  foo <- ddply(acwPrices3(), .(variable),
               function(df) {
                   df <- within(df, {
                       dt.d <- c(NA, diff(dt))
                       value.d <- c(NA, diff(value))
                       logvalue.d <- c(NA, diff(log(value)))
                   })
                   df[2:nrow(df), ]
               })
  
  within(ddply(foo, .(variable),
               summarise,
               adf=.adftest(value.d),
               pp=.pptest(value.d)),
         unitroot <- !(adf < 0.05 | pp < 0.05))
  
  within(ddply(foo, .(variable),
               summarise,
               adf=.adftest(logvalue.d),
               pp=.pptest(logvalue.d)),
         unitroot <- !(adf < 0.05 | pp < 0.05))
  
  within(ddply(foo, .(variable),
               summarise,
               adf=.adftest(logvalue.d * 7 / dt.d),
               pp=.pptest(logvalue.d * 7 / dt.d)),
         unitroot <- !(adf < 0.05 | pp < 0.05))
#+END_SRC

#+results:
| variable           |  adf |   pp | unitroot |
|--------------------+------+------+----------|
| greenback1         | 0.01 | 0.01 | FALSE    |
| greenback2         | 0.75 | 0.08 | TRUE     |
| grayback1          | 0.01 | 0.01 | FALSE    |
| grayback_houston1  | 0.01 | 0.01 | FALSE    |
| grayback_houston2  | 0.94 | 0.52 | TRUE     |
| grayback2          | 0.99 | 0.99 | TRUE     |
| grayback3          | 0.19 | 0.01 | FALSE    |
| grayback4          | 0.01 | 0.01 | FALSE    |
| csa_bond_london    | 0.07 | 0.01 | FALSE    |
| csa_bond_amsterdam | 0.48 | 0.01 | FALSE    |
| csa_bond_100mn     | 0.33 | 0.01 | FALSE    |
| csa_bond_15mn      | 0.02 | 0.04 | FALSE    |
| virginia_bond      | 0.04 | 0.01 | FALSE    |
| richmond_bond      | 0.42 | 0.11 | TRUE     |
| virginia_spread    | 0.56 | 0.01 | FALSE    |
| richmond_spread    | 0.34 | 0.08 | TRUE     |
| us_bond1           | 0.04 | 0.01 | FALSE    |
| us_bond2           | 0.01 | 0.01 | FALSE    |


Variables that are still unit roots after the first difference 

- ~greenback2~ : passes pp.test at 10 per cent
- ~richmond\_spread~ : passes pp.test at 10 per cent
- ~grayback\_houston2~ : short series
- ~grayback2~ : has lots of constant values for long periods of
  time. I think I can ignore that.

To confirm that it is the early part of the time series
that is driving the unit root behavior of the difference in 
~grayback2~

#+BEGIN_SRC R  :results output
.adftest(subset(foo, variable == "grayback2" & dt >= as.Date("1862-01-01"))[["logvalue.d"]])
.pptest(subset(foo, variable == "grayback2" & dt >= as.Date("1862-01-01"))[["logvalue.d"]])
#+END_SRC

#+results:
: [1] 0.93
: [1] 0.01

I can't reject the unit root with the ADF test, but I can easily
reject it with the Phillips-Perron test.


** Non-Missing Observations

Number of non-missings per variable.

#+BEGIN_SRC R  
  dcast(foo, variable ~ . ,
       fun.aggregate = function(x) sum(!is.na(x)))
#+END_SRC

#+results:
| variable           |  NA |
|--------------------+-----|
| greenback1         | 170 |
| greenback2         |  39 |
| grayback1          |  90 |
| grayback_houston1  |  87 |
| grayback_houston2  |   8 |
| grayback2          |  97 |
| grayback3          |  47 |
| grayback4          | 193 |
| csa_bond_london    | 111 |
| csa_bond_amsterdam |  87 |
| csa_bond_100mn     |  32 |
| csa_bond_15mn      |  25 |
| virginia_bond      |  29 |
| richmond_bond      |  25 |
| virginia_spread    |  28 |
| richmond_spread    |  24 |
| us_bond1           | 101 |
| us_bond2           | 101 |


