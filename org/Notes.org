* End of the War

http://en.wikipedia.org/wiki/Conclusion_of_the_American_Civil_War

- April 9: Surrender of Army of Northern Virginia by Lee
- April 9: Surrender of Gen. St. John Richardson Liddell's troops at
  the Battle of Fort Blakely
- April 16: Battle of Columbus. Gen James H. Wilson's Raiders capture
  Columbus, Georgia. Often called the "last battle of the Civil War"
- April 21: Disbanding of Mosby's Raiders. Never formally surrendered
  his guerrilla forces in Virginia.
- April 26: Surrender of Gen. Joseph E. Johnston surrendered
  Department of Georgia, Army of Tennessee, Department of South
  Carolina, Georgia, and Florida, and the Department of North Carolina
  and Southern Virginia. This was the largest surrender of the war,
  with almost 90,000.
- May 4. Lt. Gen. Richard Taylor surrenders deparments of Alabama,
  Mississippi, and East Louisiana regiments.
- May 5. Surrender of Confederate District of the Gulf. 
- May 10. Capture of President Jefferson Davis.
- May 11. Surrender of the Confederate Department of Florida and South Georgia.
- May 11. Surrender of Thompson's Brigade. 
- May 12. Surrender of Confederate forces of North Georgia. 
- May 13. Disbandment of forces in Texas after the battle of Palmito Ranch.
- May 26. Surrender of Kirby Smith and the Army of the
  Trans-Mississippi.  Last major Confederate force.
- November 6, 1865 Surrender of CSS Shenandoah in Great Britain.
- August 20, 1866 Johnson issues the "Proclamation Declaring that
  Peace, Order, Tranquillity [sic], and Civil Authority Now Exists in
  and Throughout the Whole of the United States of America."
