#+title: Sequential Multinomial Model of Battle Outcomes
#+BABEL: :session *R* :colnames yes
* Model 23

Daily sequential Multinomial-Dirichlet model of battle outcomes.

** Functions

#+BEGIN_SRC R :results silent
  library(ggplot2)
  library(MCMCpack) # for rdirichlet
  library(foreach)
  library(doMC)
  source("../R/load.R", chdir=TRUE)
  registerDoMC()
  
  ## Find quantiles of the dirichlet distribution.
  ## I am unaware of a closed form solution, so I use a sample.
  qdirichlet <- function(p, alpha, n=1000) {
      apply(rdirichlet(n, alpha), 2, quantile, probs=p)
  }
  
  m23.data <- acwBattleDates1()
  
  m23.estimate <- function(.data,
                           prior=c(1, 1, 1))
  {
      ret <- list()
      ret[["posterior"]] <-
          subset(transform(.data,
                           victories.CS = cumsum(victories.CS) + prior[1],
                           indecisive = cumsum(indecisive) + prior[2],
                           victories.US = cumsum(victories.US) + prior[3])
                 , , c("dt", "victories.CS", "indecisive", "victories.US"))
      ret[["prior"]] <- prior
      
      .m23summaryIter <- foreach(a=iter(ret[["posterior"]], by='row'),
                                 .combine='rbind') 
      
      ret[["summary"]] <- .m23summaryIter %do% {
          .varlist <- c("victories.CS", "indecisive", "victories.US")
          probs <- c(2.5, 97.5)
          y <- data.frame(t(qdirichlet(probs/ 100,
                                         as.numeric(a[ , .varlist]))))
          names(y) <- paste("p", probs, sep=".")
          y[["variable"]] <- .varlist
          y[["mean"]] <- with(a,
                                c(victories.CS, indecisive, victories.US) /
                                sum(victories.CS, victories.US, indecisive))
          y[["dt"]] <- a[["dt"]]
          y
      }
      ret
  }
  
#+END_SRC

** Estimate

#+BEGIN_SRC R :results silent
  ACW.DB[["m23"]] <- m23.estimate(m23.data)
#+END_SRC

** Outcome

Plot analytical expectation, and 95 percent credible intervals.

#+BEGIN_SRC R :file model23-fig1.pdf
  library(ggplot2)
  m23 <- ACW.DB[["m23"]]
  
  .gg <- (ggplot(m23[["summary"]],
                 aes(x=dt, y=mean, ymin=p.2.5, ymax=p.97.5, colour=variable))
          + geom_ribbon(alpha=0.3)
          + geom_line())
#+END_SRC

#+results:
[[file:model23-fig1.pdf]]

