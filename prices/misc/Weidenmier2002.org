* Weekly Grayback data from Weidenmier (2002)

Weekly prices of graybacks in Richmond from the Appendix of \textcite{Weidenmier2002}.

#+CAPTION: 
#+ATTR_LaTeX: longtable
| date              | price | note |
|-------------------+-------+------|
| 1861 May 31       |  1.12 |      |
| 1861 June 7       |  1.10 |      |
| 1861 June 14      |  1.12 |      |
| 1861 June 21      |  1.10 |      |
| 1861 June 28      |  1.10 |      |
| 1861 July 5       |  1.10 |      |
| 1861 July 12      |  1.10 |      |
| 1861 July 19      |  1.10 |      |
| 1861 July 26      |  1.10 |      |
| 1861 August 2     |  1.10 |      |
| 1861 August 9     |  1.12 |      |
| 1861 August 16    |  1.12 |      |
| 1861 August 23    |  1.12 |      |
| 1861 August 30    |  1.12 |      |
| 1861 September 6  |  1.12 |      |
| 1861 September 13 |  1.15 |      |
| 1861 September 20 |  1.15 | a    |
| 1861 September 27 |  1.15 | a    |
| 1861 October 4    |  1.15 | a    |
| 1861 October 11   |  1.15 |      |
| 1861 October 18   |  1.15 |      |
| 1861 October 25   |  1.20 |      |
| 1861 November 1   |  1.20 |      |
| 1861 November 8   |  1.20 |      |
| 1861 November 15  |  1.20 |      |
| 1861 November 22  |  1.20 |      |
| 1861 November 29  |  1.25 |      |
| 1861 December 6   |  1.30 |      |
| 1861 December 13  |  1.30 |      |
| 1861 December 20  |  1.35 |      |
| 1861 December 27  |  1.35 |      |
| 1862 January 3    |  1.35 |      |
| 1862 January 10   |  1.30 |      |
| 1862 January 17   |  1.30 |      |
| 1862 January 24   |  1.30 |      |
| 1862 January 31   |  1.28 |      |
| 1862 February 7   |  1.28 |      |
| 1862 February 14  |  1.40 |      |
| 1862 February 21  |  1.40 |      |
| 1862 February 28  |  1.50 |      |
| 1862 March 7      |  1.50 |      |
| 1862 March 14     |  1.50 |      |
| 1862 March 21     |  1.50 |      |
| 1862 March 28     |  1.50 |      |
| 1862 April 4      |  1.75 |      |
| 1862 April 11     |  1.76 |      |
| 1862 April 18     |  1.76 | a    |
| 1862 April 25     |  1.80 |      |
| 1862 May 2        |  2.00 |      |
| 1862 May 9        |  2.00 |      |
| 1862 May 16       |  2.00 |      |
| 1862 May 23       |  2.00 |      |
| 1862 May 30       |  2.00 |      |
| 1862 June 6       |  2.00 |      |
| 1862 June 13      |  2.00 |      |
| 1862 June 20      |  2.00 |      |
| 1862 June 27      |  2.00 |      |
| 1862 July 4       |  2.00 |      |
| 1862 July 11      |  2.00 |      |
| 1862 July 18      |  2.00 |      |
| 1862 July 25      |  2.00 |      |
| 1862 August 1     |  2.00 |      |
| 1862 August 8     |  2.00 |      |
| 1862 August 15    |  2.10 |      |
| 1862 August 22    |  2.30 |      |
| 1862 August 29    |  2.35 |      |
| 1862 September 5  |  2.40 |      |
| 1862 September 12 |  2.40 |      |
| 1862 September 19 |  2.40 |      |
| 1862 September 26 |  2.40 |      |
| 1862 October 3    |  2.40 |      |
| 1862 October 10   |  2.40 |      |
| 1862 October 17   |  2.40 |      |
| 1862 October 24   |  2.80 |      |
| 1862 October 31   |  3.50 |      |
| 1862 November 7   |  3.30 |      |
| 1862 November 14  |  3.30 |      |
| 1862 November 21  |  3.30 |      |
| 1862 November 28  |  3.25 |      |
| 1862 December 5   |  3.25 |      |
| 1862 December 12  |  3.30 |      |
| 1862 December 19  |  3.25 |      |
| 1862 December 26  |  3.25 |      |
| 1863 January 2    |  3.25 |      |
| 1863 January 9    |  3.05 |      |
| 1863 January 16   |  3.10 |      |
| 1863 January 23   |  3.20 |      |
| 1863 January 30   |  3.05 |      |
| 1863 February 6   |  3.25 |      |
| 1863 February 13  |  3.30 |      |
| 1863 February 20  |  3.30 |      |
| 1863 February 27  |  3.40 |      |
| 1863 March 6      |  4.50 |      |
| 1863 March 13     |  5.00 |      |
| 1863 March 20     |  5.25 |      |
| 1863 March 27     |  5.25 |      |
| 1863 April 3      |  5.00 |      |
| 1863 April 10     |  5.50 |      |
| 1863 April 17     |  5.75 |      |
| 1863 April 24     |  5.50 |      |
| 1863 May 1        |  5.00 |      |
| 1863 May 8        |  6.50 |      |
| 1863 May 15       |  5.25 |      |
| 1863 May 22       |  6.00 |      |
| 1863 May 29       |  6.25 |      |
| 1863 June 5       |  7.00 |      |
| 1863 June 12      |  8.00 |      |
| 1863 June 19      |  8.50 |      |
| 1863 June 26      |  8.50 |      |
| 1863 July 3       |  8.25 |      |
| 1863 July 10      |  7.50 |      |
| 1863 July 17      | 10.00 |      |
| 1863 July 24      | 11.00 |      |
| 1863 July 31      | 11.00 |      |
| 1863 August 7     | 12.00 |      |
| 1863 August 14    | 13.00 |      |
| 1863 August 21    | 13.00 | a    |
| 1863 August 28    | 12.00 |      |
| 1863 September 4  | 12.00 |      |
| 1863 September 11 | 13.00 |      |
| 1863 September 18 | 12.50 |      |
| 1863 September 25 | 11.50 |      |
| 1863 October 2    | 12.50 |      |
| 1863 October 9    | 12.50 |      |
| 1863 October 16   | 11.50 |      |
| 1863 October 23   | 12.50 |      |
| 1863 October 30   | 12.50 |      |
| 1863 November 6   | 12.50 |      |
| 1863 November 13  | 15.00 |      |
| 1863 November 20  | 16.75 |      |
| 1863 November 27  | 17.00 |      |
| 1863 December 4   | 17.00 |      |
| 1863 December 11  | 21.00 |      |
| 1863 December 18  | 18.00 |      |
| 1863 December 25  | 20.00 |      |
| 1864 January 1    | 20.00 |      |
| 1864 January 8    | 22.00 |      |
| 1864 January 15   | 22.00 |      |
| 1864 January 22   | 21.00 |      |
| 1864 January 29   | 20.50 |      |
| 1864 February 5   | 22.00 |      |
| 1864 February 12  | 22.50 |      |
| 1864 February 19  | 22.00 |      |
| 1864 February 26  | 27.00 |      |
| 1864 March 4      | 27.50 |      |
| 1864 March 11     | 22.00 |      |
| 1864 March 18     | 22.00 |      |
| 1864 March 25     | 21.25 |      |
| 1864 April 1      | 23.00 |      |
| 1864 April 8      | 23.00 |      |
| 1864 April 15     | 26.00 |      |
| 1864 April 22     | 23.00 |      |
| 1864 April 29     | 21.00 |      |
| 1864 May 6        | 21.25 | b    |
| 1864 May 13       | 18.00 | b    |
| 1864 May 20       | 18.00 | b    |
| 1864 May 27       | 16.00 | b    |
| 1864 June 3       | 17.00 | b    |
| 1864 June 10      | 16.00 | b    |
| 1864 June 17      | 16.00 | b    |
| 1864 June 24      | 17.00 | b    |
| 1864 July 1       | 17.00 | b    |
| 1864 July 8       | 17.00 | b    |
| 1864 July 15      | 17.00 | b    |
| 1864 July 22      | 18.00 | b    |
| 1864 July 29      | 19.00 | b    |
| 1864 August 5     | 18.00 | b    |
| 1864 August 12    | 19.00 | b    |
| 1864 August 19    | 19.00 | b    |
| 1864 August 26    | 20.00 | b    |
| 1864 September 2  | 20.00 |      |
| 1864 September 9  | 23.00 |      |
| 1864 September 16 | 25.00 |      |
| 1864 September 23 | 25.00 |      |
| 1864 September 30 | 25.00 |      |
| 1864 October 7    | 25.00 | b    |
| 1864 October 14   | 23.00 | b    |
| 1864 October 21   | 23.00 | b    |
| 1864 October 28   | 26.00 | b    |
| 1864 November 4   | 25.00 |      |
| 1864 November 11  | 28.00 |      |
| 1864 November 18  | 28.00 |      |
| 1864 November 25  | 28.00 |      |
| 1864 December 2   | 29.00 |      |
| 1864 December 9   | 34.50 |      |
| 1864 December 16  | 34.75 |      |
| 1864 December 23  | 36.00 | b    |
| 1864 December 30  | 40.00 |      |
| 1865 January 6    | 49.50 |      |
| 1865 January 13   | 50.00 |      |
| 1865 January 20   | 66.50 |      |
| 1865 February 3   | 45.00 |      |
| 1865 February 10  | 45.00 | b    |
| 1865 February 17  | 45.00 | b    |

Grayback/gold price quotations were taken from the following
newspapers:Richmond Dispatch, Richmond Examiner, Richmond Whig,
Richmond Enquirer, Petersburg Express, WilmingtonJournal, Columbus
Daily-Sun, CharlestonMercury,LynchburgVirginian,and the Mobile
Tribune.Richmondgrayback/gold pricequotationswere also
cross-checkedagainstprices in the Raleigh, Augusta,and Mobile gold
marketsusing the following newspapers: Raleigh WeeklyRegister, Mobile
Tribune,Mobile Advertiserand Register, and Augusta Daily
Constitutionalist.

Notes 

- *a* Grayback/goldprice quotationswere unavailablefor these
  particular weeks. The graybackprice of gold has been interpolated
  these particular dates assuminga constantprice from the last
  available trade.
- *b* Prices were not reported in the Richmond newspapers for these
  particular dates. The gaps were filled in with grayback / gold price
  quotationsfrom the Wilmington,North Carolina,money market.  All
  Wilmington gold prices quotations are taken from the Wilmington Daily
  Journal.

Other notes

- Duplicate April 1 1864 in original table. Edited second observation
  to April 8, 1864.

* References

#+BEGIN_LATEX
\printbibliography
#+END_LATEX

