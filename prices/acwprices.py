#!/usr/bin/env python2.6
"""Create American Civil War Prices database
"""
from datetime import datetime as datetime_
from datetime import date as date_
import csv
import re
import sys

import sqlalchemy as sa
from sqlalchemy.ext import declarative

Base = declarative.declarative_base()

def _recast(x, f):
    """return f(x) or None"""
    try:
        y = f(x)
    except:
        y = None
    return y

def _numeric(x):
    """Convert to float"""
    return _recast(x, float)

def _toint(x):
    """Convert to float"""
    return _recast(x, int)

def _date(x, format):
    """Convert to datetime.Date"""
    return _recast(x, lambda y: datetime_.strptime(y, format).date())

def _tsv(x):
    """Split tab-separated string into a list"""
    return x.strip().split('\t')

def _biweekly(start_date, end_date):
    """Return 1st and 15th dates between start_date and end_date"""
    ret = []
    d = start_date
    while d < end_date:
        ret.append(d)
        if d.day == 15:
            if d.month < 12:
                mm = d.month + 1
                yyyy = d.year
            else:
                newmon = 1
                yyyy = d.year + 1
            d = d.replace(year=yyyy, month=mm, day=1)
        else:
            d = d.replace(day=15)
    return ret

class Mixin(object):
    @classmethod
    def insert(cls, *args, **kwargs):
        """Insert method for the __table__"""
        return cls.__table__.insert(*args, **kwargs)

    @classmethod
    def colnames(cls):
        """Column names of the __table__"""
        return cls.__table__.columns.keys()

    @classmethod
    def load(cls, file):
        """Load data from raw file into the table"""
        result = cls.insert().execute(cls._loaddata(file))
        print("%d rows added to table %s" % (result.rowcount,
                                             cls.__tablename__))
        return result

    @classmethod
    def _loaddata(cls, file):
        pass


class Greenback1(Base, Mixin):
    """Greenback prices

    Mitchell's (1903) series of the price of a gold dollar in
    greenbacks.
    """
    __tablename__ = 'greenback1'

    dt = sa.Column(sa.Date, primary_key=True)
    high = sa.Column(sa.Numeric)
    low = sa.Column(sa.Numeric)

    @classmethod
    def _loaddata(cls, file):
        with open(file, 'r') as f:
            # drop header
            data = f.readlines()[6:]
        def _f(x):
            y = re.sub('^ +', '', x).strip()
            y = re.split(" +", y)
            y = dict(zip(cls.colnames(), (y[1], y[3], y[4])))
            y['dt'] = _date('18' + y['dt'], '%Y%m%d')
            return y
        return [ _f(x) for x in data]

class Greenback2(Base, Mixin):
    """Greenback prices (Todd)"""
    __tablename__ = 'greenback2'
    dt = sa.Column(sa.Date, primary_key=True)
    price = sa.Column(sa.Numeric)

    @classmethod
    def _loaddata(cls, file):
        with open(file, 'r') as f:
            data = f.readlines()[1:]
        def _f(x):
            y = dict(zip(cls.colnames(), x.split('\t')))
            y['dt'] = _date(y['dt'], '%Y-%m-%d')
            return y
        return [_f(x) for x in data]

class Grayback1(Base, Mixin):
    """Grayback price series (Weidermier)

    Weidermier's series of the price of a gold dollar in Confederate paper
    dollars.
    """
    __tablename__ = 'grayback1'

    dt = sa.Column(sa.Date, primary_key=True)
    richmond = sa.Column(sa.Numeric)
    houston_old = sa.Column(sa.Numeric)
    houston_new = sa.Column(sa.Numeric)

    @classmethod
    def _loaddata(cls, file):
        with open(file, 'r') as f:
            data = f.readlines()[1:]
        data = [ dict(zip(cls.colnames(), x.split(','))) for x in data ]
        for x in data:
            x['dt'] = datetime_.strptime(x['dt'], '"%Y/%m/%d"').date()
            for i in ('richmond', 'houston_old', 'houston_new'):
                x[i] = _numeric(x[i])
        return data

class ConfederateEuroBonds(Base, Mixin):
    """Confederate bonds issued in London and Amsterdam"""

    __tablename__ = 'csa_bonds_europe'

    dt = sa.Column(sa.Date, primary_key=True)
    london = sa.Column(sa.Numeric)
    amsterdam = sa.Column(sa.Numeric)

    @classmethod
    def _loaddata(cls, file):
        with open(file, 'r') as f:
            data = f.readlines()[1:]
        data = [ dict(zip(cls.colnames(), x.split(','))) for x in data ]
        for x in data:
            x['dt'] = datetime_.strptime(x['dt'].replace(" ",""), '"%Y/%m/%d"').date()
            for i in ('london', 'amsterdam'):
                x[i] = _numeric(x[i])
        return data

class Grayback2(Base, Mixin):
    """Grayback price series (McCandless)"""
    __tablename__ = 'grayback2'
    dt = sa.Column(sa.Date, primary_key=True)
    price = sa.Column(sa.Numeric)

    @classmethod
    def _loaddata(cls, file):
        with open(file, 'r') as f:
            data = f.readlines()[1:]
        ret = []
        _fmt = '%Y-%m-%d'
        for x in data:
            y = _tsv(x)
            if y[1] == '':
                obs = {'dt': _date(y[0], _fmt),
                     'price': _numeric(y[2])}
                ret.append(obs)
            else:
                for dt in _biweekly(*[_date(x, _fmt) for x in y[0:2]]):
                    obs = {'dt' : dt,
                           'price' : _numeric(y[2])}
                    ret.append(obs)
        return ret

class Grayback3(Base, Mixin):
    """Grayback price series (Todd)"""
    __tablename__ = 'grayback3'
    dt = sa.Column(sa.Date, primary_key=True)
    mean = sa.Column(sa.Numeric)
    low = sa.Column(sa.Numeric)
    high = sa.Column(sa.Numeric)

    @classmethod
    def _loaddata(cls, file):
        def _g(x):
            y = dict(zip(cls.colnames(), _tsv(x)))
            y['dt'] = _date(y['dt'], '%Y-%m-%d')
            for col in cls.colnames()[1:]:
                if col in y:
                    y[col] = _numeric(y[col])
                else:
                    y[col] = None
            return y
        with open(file, 'r') as f:
            data = [_g(x) for x in f.readlines()[1:]]
        return data

class ConfederateBonds(Base, Mixin):
    """Confederate bonds issued domestically (Davis and Pecquet)"""
    __tablename__ = 'csa_bonds'
    dt = sa.Column(sa.Date, primary_key=True)
    bond100 = sa.Column(sa.Numeric)
    bond15 = sa.Column(sa.Numeric)
    richmond = sa.Column(sa.Numeric)
    virginia = sa.Column(sa.Numeric)
    rate = sa.Column(sa.Numeric)
    gold = sa.Column(sa.Numeric)

    @classmethod
    def _loaddata(cls, file):
        def _g(x):
            y = dict(zip(cls.colnames(), _tsv(x)))
            y['dt'] = _date(y['dt'], '%Y-%m-%d')
            for col in cls.colnames()[1:]:
                if col in y:
                    y[col] = _numeric(y[col])
                else:
                    y[col] = None
            return y
        with open(file, 'r') as f:
            data = [_g(x) for x in f.readlines()[1:]]
        return data


class SterlingDollar1(Base, Mixin):
    """Sterling Dollar exchange rates (Officer)"""
    __tablename__ = 'sterling_dollar_1'
    dt = sa.Column(sa.Date, primary_key=True)
    premium = sa.Column(sa.Numeric)

    @classmethod
    def _loaddata(cls, file):
        qtrs = (1, 4, 7, 10)
        data = []
        with open(file, 'r') as f:
            for row in f.readlines()[1:]:
                xlist = _tsv(row)
                yyyy = int(xlist[0])
                for i in range(1, 5):
                    dt = date_(yyyy, qtrs[i - 1], 1)
                    data.append({'dt': dt, 'premium': xlist[i]})
        return data

class SterlingDollar2(Base, Mixin):
    """Sterling-Dollar exchange rates, Boston series (Martin)"""
    __tablename__ = 'sterling_dollar_2'
    dt = sa.Column(sa.Date, primary_key=True)
    low = sa.Column(sa.Numeric)
    high = sa.Column(sa.Numeric)

    @classmethod
    def _loaddata(cls, file):
        data = []
        with open(file, 'r') as f:
            for row in f.readlines()[1:]:
                xlist = _tsv(row)
                dt = date_(int(xlist[0]), int(xlist[1]), 1)
                data.append({'dt': dt, 'low': xlist[2], 'high': xlist[3]})
        return data

class Roll1972Tbl1(Base, Mixin):
    """Roll (1972) United States Interest Rate Yields, Table 1"""
    __tablename__ = 'roll1972_tbl1'
    bond = sa.Column(sa.Unicode, primary_key=True)
    terms = sa.Column(sa.Integer)
    price = sa.Column(sa.Numeric)

    @classmethod
    def _loaddata(cls, file):
        data = []
        with open(file, 'r') as f:
            reader = csv.DictReader(f, delimiter='\t')
            for row in reader:
                row['bond'] = unicode(row['bond'])
                row['terms'] = _toint(row['terms'])
                data.append(row)
        return data

class Roll1972Tbl3(Base, Mixin):
    """Roll (1972) United States Interest Rate Yields"""
    __tablename__ = 'roll1972_tbl3'
    dt = sa.Column(sa.Date, primary_key=True)
    yield_greenback_1yr = sa.Column(sa.Numeric)
    gold_price_change = sa.Column(sa.Numeric)
    yield1040_10yr = sa.Column(sa.Numeric)
    yield1040_1879 = sa.Column(sa.Numeric)
    yield1040_40 = sa.Column(sa.Numeric)
    yield_sixes1881 = sa.Column(sa.Numeric)
        
    @classmethod
    def _loaddata(cls, file):
        data = []
        with open(file, 'r') as f:
            reader = csv.DictReader(f, delimiter='\t')
            for row in reader:
                row['dt'] = _date(row['date'], "%Y-%m-%d")
                del row['date']
                row['yield_sixes1881'] = _numeric(row['yield_sixes1881'])
                row['gold_price_change'] = _numeric(row['gold_price_change'])
                data.append(row)
        return data

class Roll1972Tbl4(Base, Mixin):
    """Roll (1972) United States Interest Rate Yields"""
    __tablename__ = 'roll1972_tbl4'
    dt = sa.Column(sa.Date, primary_key=True)
    yield_greenback_1yr = sa.Column(sa.Numeric)
    gold_price_change = sa.Column(sa.Numeric)
    yield_sixes1881 = sa.Column(sa.Numeric)
        
    @classmethod
    def _loaddata(cls, file):
        data = []
        with open(file, 'r') as f:
            reader = csv.DictReader(f, delimiter='\t')
            for row in reader:
                row['dt'] = _date(row['date'], "%Y-%m-%d")
                del row['date']
                row['yield_sixes1881'] = _numeric(row['yield_sixes1881'])
                data.append(row)
        return data

class Grayback4(Base, Mixin):
    """Weekly Grayback series from Weidermier 2002"""
    
    __tablename__ = 'grayback4'
    dt = sa.Column(sa.Date, primary_key=True)
    price = sa.Column(sa.Numeric)
    note = sa.Column(sa.Unicode(1))

    @classmethod
    def _loaddata(cls, file):
        data = []
        with open(file, 'r') as f:
            reader = csv.DictReader(f, delimiter='\t')
            for row in reader:
                row['dt'] = _date(row['date'], "%Y %B %d")
                del row['date']
                row['note'] = unicode(row['note'])
                data.append(row)
        return data
    
def load_all(DB):
    Base.metadata.bind = sa.create_engine('sqlite:///%s' % DB)
    print("Using database %s" % Base.metadata.bind.engine)

    # clean up
    Base.metadata.drop_all()
    Base.metadata.create_all()
    
    Greenback1.load('./eh.net/greenback.txt')
    Greenback2.load('./misc/Todd1954_Appendix_C1.tsv')
    Grayback1.load('./eh.net/grayback.csv')
    Grayback2.load('./misc/McCandless1996_grayback.tsv')
    Grayback3.load('./misc/Todd1954_Appendix_C2.tsv')
    Grayback4.load('./misc/Weidenmier2002.tsv')
    ConfederateEuroBonds.load('./eh.net/confederate_bonddata.csv')
    ConfederateBonds.load('./misc/DavisPequet1990.tsv')
    SterlingDollar1.load('./misc/Officer_Table1.tsv')
    SterlingDollar2.load('./misc/Martin_1862-1870.tsv')
    Roll1972Tbl1.load('./misc/Roll1972_Table1.tsv')
    Roll1972Tbl3.load('./misc/Roll1972_Table3.tsv')
    Roll1972Tbl4.load('./misc/Roll1972_Table4.tsv')

def main():
    # Name of database
    DB = sys.argv[1]
    load_all(DB)
    
if __name__ == '__main__':
    foo = main()
